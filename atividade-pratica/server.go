package main

import ( "net/http"
		 "time" 
		 "log"
		 "github.com/gorilla/mux"
		 "gopkg.in/go-playground/validator.v9")


var Users = []User{User{"cebola@roxa.com", "123456", "21/08/1970"}}

var val = validator.New()

func main() {
	StartServer()
	
	// Essa linha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()


	s := r.PathPrefix("/register").Subrouter()
	h := s.Methods("POST").Subrouter()
	h.HandleFunc("",Register).Methods("POST").HeadersRegexp("Content-Type", "application/json")
	h.HandleFunc("",ErrorHeader)
	s.HandleFunc("",ErrorMethod)

	s2 := r.PathPrefix("/login").Subrouter()
	h2 := s2.Methods("POST").Subrouter()
	h2.HandleFunc("",Login).Methods("POST").HeadersRegexp("Content-Type", "application/json")
	h2.HandleFunc("",ErrorHeader)
	s2.HandleFunc("",ErrorMethod)

	

	server := &http.Server{
			Addr       : "192.168.15.8:8084",
			IdleTimeout: duration,
			Handler    : r, 
	}

	log.Print(server.ListenAndServe())
}

