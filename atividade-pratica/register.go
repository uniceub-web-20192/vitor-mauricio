package main
import ( "net/http"
		 "log"
		 "fmt"
		 "io/ioutil"
		 "encoding/json")

func Register(res http.ResponseWriter, req *http.Request) {

	var u User
	body, _ := ioutil.ReadAll(req.Body)

	json.Unmarshal(body, &u)
	err := val.Struct(u)
	_, sucID := CatchId(u.Email)

	if err != nil{
		log.Printf("Validation error: %v",err)
		res.Write([]byte("{message: invalid body}"))
		res.Header().Set("status","400")
		return
	}
	if sucID{
		res.Write([]byte("{message: this email already exist}"))
		res.Header().Set("status","403")
		return
	}
	Users = append(Users,u)
	res.Write([]byte("{message: user successfully registered}"))
	res.Header().Set("status","200")

	fmt.Printf("Usuários Cadastrados: %v\n", Users)
}