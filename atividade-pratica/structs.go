package main

type User struct {

	Email   string `json:"email" validate:"required,email"` 
	Pass  string `json:"pass" validate:"required"`
	Birthdate   string `json:"birthdate" validate:"required"` 
}

type LoginJson struct{
	Email   string `json:"email" validate:"required,email"` 
	Pass  string `json:"pass" validate:"required"`
}
