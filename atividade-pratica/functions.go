package main
import ( "net/http"
		 "fmt"
		 "time" 
		 "strings"
		 "strconv")

func ErrorMethod(res http.ResponseWriter, req *http.Request) {
	
	h := req.Method
	if req.Method != "POST"{
		res.Header().Set("status","405")
    fmt.Printf("Usuários Cadastrados: %v Metodo:%v\n", Users,h)
	}
	//res.Write([]byte(h))
}
func ErrorHeader(res http.ResponseWriter, req *http.Request) {
	
	h := req.Method
	res.Header().Set("status","400")
    fmt.Printf("Usuários Cadastrados: %v Metodo:%v\n", Users,h)
	res.Write([]byte("{message: invalid header}"))
}

func CatchId (email string) (int, bool){
	id := 0
	sucess := false
	for i := 0; i < len(Users); i++ {
		if email == Users[i].Email{
			id = i
			sucess = true
			return id,sucess
		}
	}
	return id,sucess
}
func VerifyPass(id int, pass string) bool{
	sucess := false
	if Users[id].Pass == pass{
		sucess = true
	}
	return sucess
}
func AgeCalc(id int)string{
	t := time.Now()
	year := t.Year()

	date := strings.Split(Users[id].Birthdate,"/")
	y, _ := strconv.Atoi(date[2])
	age := strconv.Itoa(year - y)
	return age
}