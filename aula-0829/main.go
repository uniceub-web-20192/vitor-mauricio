package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {

	StartServer()

	// Essa linha deve ser executada sem alteração
	// da função StartServer
	log.Println("[INFO] Servidor no ar!")
}

func cebolas(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("TESTE"))
}
func teste(w http.ResponseWriter, r *http.Request) {
	//w.Write([]byte(r.Method + " " + r.Host + r.URL.Path))
	q := r.URL.RawQuery
	fmt.Fprintf(w, r.Method+" "+r.URL.Path+"?"+q)
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
		Addr:        "172.22.51.160:8086",
		IdleTimeout: duration,
	}

	http.HandleFunc("/cebola", cebolas)
	http.HandleFunc("/", teste)

	log.Print(server.ListenAndServe())
}
