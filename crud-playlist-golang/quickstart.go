// Sample Go code for user authorization

package main

import (
  "encoding/json"
  "fmt"
  "log"
  "io/ioutil"
  "net/http"
  "net/url"
  "os"
  "os/user"
  "path/filepath"

  "golang.org/x/net/context"
  "golang.org/x/oauth2"
  "golang.org/x/oauth2/google"
  "google.golang.org/api/youtube/v3"
)

const missingClientSecretsMessage = `
Please configure OAuth 2.0
`

// getClient uses a Context and Config to retrieve a Token
// then generate a Client. It returns the generated Client.
func getClient(ctx context.Context, config *oauth2.Config) *http.Client {
  cacheFile, err := tokenCacheFile()
  if err != nil {
    log.Fatalf("Unable to get path to cached credential file. %v", err)
  }
  tok, err := tokenFromFile(cacheFile)
  if err != nil {
    tok = getTokenFromWeb(config)
    saveToken(cacheFile, tok)
  }
  return config.Client(ctx, tok)
}

// getTokenFromWeb uses Config to request a Token.
// It returns the retrieved Token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
  authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
  fmt.Printf("Go to the following link in your browser then type the "+
    "authorization code: \n%v\n", authURL)

  var code string
  if _, err := fmt.Scan(&code); err != nil {
    log.Fatalf("Unable to read authorization code %v", err)
  }

  tok, err := config.Exchange(oauth2.NoContext, code)
  if err != nil {
    log.Fatalf("Unable to retrieve token from web %v", err)
  }
  return tok
}

// tokenCacheFile generates credential file path/filename.
// It returns the generated credential path/filename.
func tokenCacheFile() (string, error) {
  usr, err := user.Current()
  if err != nil {
    return "", err
  }
  tokenCacheDir := filepath.Join(usr.HomeDir, ".credentials")
  os.MkdirAll(tokenCacheDir, 0700)
  return filepath.Join(tokenCacheDir,
    url.QueryEscape("youtube-go-quickstart.json")), err
}

// tokenFromFile retrieves a Token from a given file path.
// It returns the retrieved Token and any read error encountered.
func tokenFromFile(file string) (*oauth2.Token, error) {
  f, err := os.Open(file)
  if err != nil {
    return nil, err
  }
  t := &oauth2.Token{}
  err = json.NewDecoder(f).Decode(t)
  defer f.Close()
  return t, err
}

// saveToken uses a file path to create a file and store the
// token in it.
func saveToken(file string, token *oauth2.Token) {
  fmt.Printf("Saving credential file to: %s\n", file)
  f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
  if err != nil {
    log.Fatalf("Unable to cache oauth token: %v", err)
  }
  defer f.Close()
  json.NewEncoder(f).Encode(token)
}

func handleError(err error, message string) {
  if message == "" {
    message = "Error making API call"
  }
  if err != nil {
    log.Fatalf(message + ": %v", err.Error())
  }
}

func channelsListByUsername(service *youtube.Service, part string, forUsername string) {
  call := service.Channels.List(part)
  call = call.ForUsername(forUsername)
  response, err := call.Do()
  handleError(err, "")
  fmt.Println(fmt.Sprintf("This channel's ID is %s. Its title is '%s', " +
              "and it has %d views.",
              response.Items[0].Id,
              response.Items[0].Snippet.Title,
              response.Items[0].Statistics.ViewCount))
}

func createPlaylist(service *youtube.Service, part string, title string, pub int ){
	status := "public"
	if pub == 1{
		status = "public"
	}else{
		status = "private"
	}
	p := &youtube.Playlist{
		Snippet: &youtube.PlaylistSnippet{
			Title:       title,
		},
		Status: &youtube.PlaylistStatus{PrivacyStatus: status},
	}
	call := service.Playlists.Insert(part,p)
	_, err := call.Do()
	handleError(err, "")
	fmt.Println(fmt.Sprintf("Playlist criada com sucesso!\n"))
}

func listPlaylist(service *youtube.Service, part string) []*youtube.Playlist{
	call := service.Playlists.List(part).Mine(true)
	response, err := call.Do()
	handleError(err, "")
	for x, playlist := range response.Items {
		playlistId := playlist.Id
		playlistTitle := playlist.Snippet.Title

		fmt.Println("[",x+1,"]", "----","ID: ", playlistId, "---- Name: ", playlistTitle)
	}
	return response.Items
}
func deletePlaylist(service *youtube.Service, id string){
	call := service.Playlists.Delete(id)
	err := call.Do()
	handleError(err, "")
}

func updatePlaylist(service *youtube.Service, part string, id string, title string ){
	p := &youtube.Playlist{
		Id : id,
		Snippet: &youtube.PlaylistSnippet{
			Title:       title,
		},
	}
	call := service.Playlists.Update(part,p)
	_, err := call.Do()
	handleError(err, "")
}
func main() {
  ctx := context.Background()

  b, err := ioutil.ReadFile("client_secret.json")
  if err != nil {
    log.Fatalf("Unable to read client secret file: %v", err)
  }

  // If modifying these scopes, delete your previously saved credentials
  // at ~/.credentials/youtube-go-quickstart.json
  config, err := google.ConfigFromJSON(b, youtube.YoutubeScope)
  if err != nil {
    log.Fatalf("Unable to parse client secret file to config: %v", err)
  }
  client := getClient(ctx, config)
  service, err := youtube.New(client)
  handleError(err, "Error creating YouTube client")



  //MENU
  var option string
  playlists := listPlaylist(service, "snippet")
  fmt.Println(playlists)
  var deleteId int
  var nameP string
  var pub int
  var updateId int
  var nameU string
  for{
	
	fmt.Println("_______________________________________________________\n[1]- Delete playlist\n[2]- Update playlist\n[3]- Create playlist\n[4]- Exit")
	fmt.Scan(&option)
	if option == "1"{
		
		fmt.Printf("Digite o indíce da playlist que quer apagar: ")
		fmt.Scan(&deleteId)
		deletePlaylist(service,playlists[deleteId-1].Id)
	}else if option == "2"{
		
		fmt.Printf("Digite o indíce da playlist que quer atualizar: ")
		fmt.Scan(&updateId)
		fmt.Printf("Digite o novo nome da playlist: ")
		fmt.Scan(&nameU)
		updatePlaylist(service,"snippet",playlists[updateId-1].Id,nameU)
	}else if option == "3"{
		
		fmt.Printf("Digite o nome da playlist: ")
		fmt.Scan(&nameP)
		fmt.Printf("A playlist será publica[1] ou privada[2]?")
		fmt.Scan(&pub)
		createPlaylist(service, "snippet,status", nameP,pub)
	}else if option == "4"{
		return
	}
	playlists = listPlaylist(service, "snippet")
  }  
  

  //channelsListByUsername(service, "snippet,contentDetails,statistics", "GoogleDevelopers")
  //O nome da playlist não pode conter espaços (arrumar)

}